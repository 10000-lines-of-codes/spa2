// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import VueRouter from 'vue-router'
import Hello from './components/HelloWorld'
import Danhmucma from './components/Danhmucma'
import Giaodich from './components/Giaodich'
import Lichsu from './components/Lichsu'
import GDtotnhat from './components/GDtotnhat'
import Ruttien from './components/Ruttien'
import Khuyenmai from './components/Khuyenmai'
import Media from './components/Media'
import Napthem from './components/Napthem'
import Dautu from './components/Dautu'

Vue.use(VueRouter)

const routes = [
    {
	path: '/', component: Hello
    },
    {
	path: '/danhmucma', component: Danhmucma
    },
    {
	path: '/giaodich', component: Giaodich 
    },
    {
	path: '/lichsu', component: Lichsu 
    },
    {
	path: '/gdtotnhat', component: GDtotnhat
    },
    {
	path: '/ruttien', component: Ruttien 
    },
    {
	path: '/khuyenmai', component: Khuyenmai
    },
    {
	path: '/media', component: Media 
    },
    {
	path: '/napthem', component: Napthem 
    },
    {
	path: '/dautu', component: Dautu 
    },
    

]

const router = new VueRouter({
    routes,
    mode: 'history'
})

router.beforeEach((to, from, next) => {
    if(to.path == '/param') {
	if(localStorage.getItem('user')==undefined){
	    var user = prompt('enter username');
	    var pass = prompt('enter password');
	    if (user == 'username' && pass == 'password') {
		localStorage.setItem('user', user);
		next();
	    } else {
		alert('wrong')
		return;
	    }
	}
    }
    next()
})
	    

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}).$mount('#app')
